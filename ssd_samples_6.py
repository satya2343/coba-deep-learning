from io import StringIO
import io
import PIL
import numpy
import numpy as np
import skimage as sk
from skimage import transform
from skimage import util
import generate_number
import cv2 as cv2
import matplotlib.pyplot as plt
from PIL import ImageFont, ImageDraw, Image
from six.moves import cPickle as pickle
import os
import pandas as pd
import json
import h5py
import time

def create_plate_background(height, width):
    # CREATE CANVAS
    background = np.zeros((height, width, 3), np.uint8)
    c = np.random.randint(2, 100)
    color = np.array([c, c, c], np.uint8)
    background += color
    return background

def insert_text_to_plate_background(plate_background, plate_number, *year):
    font_path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/IndonesiaLicensePlate.ttf"
    font_size = 32
    font = ImageFont.truetype(font_path, font_size)
    c = np.random.randint(120, 255)
    color = (c, c, c, 0)

    # CREATE IMAGE
    img = Image.fromarray(plate_background)
    draw = ImageDraw.Draw(img)

    # Get the width of the text for text alignment
    text_size_x, text_size_y = draw.textsize(plate_number, font=font)
    # Center the plate number text
    draw.text(((width - text_size_x) / 2, 2), plate_number, font=font, fill=color)

    if year[0]:
        font_size = 14
        font = ImageFont.truetype(font_path, font_size)

        # Get the width of the text for text alignment
        text_size_x, text_size_y = draw.textsize(plate_number, font=font)
        # randomize the year number text location between left, center, right
        draw.text((np.random.randint(5, (width - text_size_x)), 28), str(year[0]), font=font, fill=color)

    # PAD IMAGE WITH ALL WHITE random border
    img = np.array(img)
    img = np.pad(img, ((1, 1), (1, 1), (0, 0)), 'constant', constant_values=np.random.randint(0, 255))

    return img

def random_pt_1_side(pad_x=None, pad_y=None, new_width=None, new_height=None, coeff=None):

    if np.random.randint(1, 4) == 1:
        pt2 = np.float32([[pad_x + np.random.randint(-coeff, coeff), pad_y + np.random.randint(-coeff, coeff)],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif np.random.randint(1, 4) == 2:
        pt2 = np.float32([[pad_x , pad_y],
                          [pad_x + new_width + np.random.randint(-coeff, coeff), pad_y + np.random.randint(-coeff, coeff)],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif np.random.randint(1, 4) == 3:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x + np.random.randint(-coeff, coeff), pad_y + new_height + np.random.randint(-coeff, coeff)],
                          [pad_x + new_width, pad_y + new_height]])
    else:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width + np.random.randint(-coeff, coeff), pad_y + new_height + np.random.randint(-coeff, coeff)]])
    return pt2


def modify_image_plate(plate, pad, coeff):
    width, height, channels = plate.shape
    img = Image.fromarray(plate)

    # SCALE IMAGE
    scaling = np.random.uniform(0.7, 1.2)
    new_height = int(height * scaling)
    new_width = int(width * scaling)
    img = img.resize((new_height, new_width))

    img = np.array(img)
    #APPLYING SKEWING TRANSFORM :V

    """
    Skewing transformation menggunakan nilai coeff yang random untuk tiap sisi
    SSD transformation need variant size of the image
    In real data it will represent the range of the plate 
    """
    # Pad the image
    pad_x = pad
    pad_y = pad

    container = np.zeros((img.shape[0]+ 2*pad_x, img.shape[1] + 2*pad_y, 3), dtype=np.uint8 )
    container[pad_x: pad_x + img.shape[0], pad_y: pad_y + img.shape[1]] = img
    img = container

    pt1 = np.float32([[pad_x, pad_y],                           # Kiri atas
                      [pad_x + new_width, pad_y],               # Kanan atas
                      [pad_x, pad_y + new_height],              # Kiri bawah
                      [pad_x + new_width, pad_y + new_height]])  # Kanan bawah

    coeff = coeff
    pt2 = random_pt_1_side(pad_x, pad_y, new_width, new_height, coeff)
    M = cv2.getPerspectiveTransform(pt1, pt2)
    img = cv2.warpPerspective(img, M, (out_perspective_width, out_perspective_height))
    """Here we apply the random resize of the plate number"""
    scaling = np.random.uniform(0.4, 1.1)
    new_height = int(out_perspective_height * scaling)
    new_width = int(out_perspective_width * scaling)
    img = cv2.resize(img, (new_width, new_height))
    # ROTATE IMAGE
    # img = Image.fromarray(img)
    # img = img.rotate(np.random.randint(-30, 30), Image.NEAREST, True)

    img = np.array(img)
    return img

def find_edge_point(temp, edge):
    w = temp.shape[1]
    h = temp.shape[0]
    # Find y_min
    if edge == 'y_min':
        for i in range(h):
            for j in range(w):
                if temp[i, j] != 0:
                    y_min = temp[i, j]
                    return i
    # Find y_max
    if edge == 'y_max':
        for i in reversed(range(h)):
            for j in reversed(range(w)):
                if temp[i, j] != 0:
                    y_max = temp[i, j]
                    return i
    # Find x_min
    if edge == 'x_min':
        for j in range(w):
            for i in range(h):
                if temp[i, j] != 0:
                    x_min = temp[i, j]
                    return j
    # Find x_max
    if edge == 'x_max':
        for j in reversed(range(w)):
            for i in reversed(range(h)):
                if temp[i, j] != 0:
                    x_max = temp[i, j]
                    return j

def find_plate_edges(img):
    img = np.array(img)
    temp = np.sum(img, axis=2)

    y_min = find_edge_point(temp, 'y_min')
    x_min = find_edge_point(temp, 'x_min')
    y_max = find_edge_point(temp, 'y_max')
    x_max = find_edge_point(temp, 'x_max')

    return y_min, y_max, x_min, x_max


def modify_image_plate2(plate, pad, coeff):
    width, height, channels = plate.shape
    img = Image.fromarray(plate)

    # SCALE IMAGE
    scaling = np.random.uniform(0.7, 1.2)
    new_height = int(height * scaling)
    new_width = int(width * scaling)
    img = img.resize((new_height, new_width))

    img = np.array(img)
    #APPLYING SKEWING TRANSFORM :V

    """
    Skewing transformation menggunakan nilai coeff yang random untuk tiap sisi
    SSD transformation need variant size of the image
    In real data it will represent the range of the plate 
    """
    # Pad the image
    pad_x = pad
    pad_y = pad

    container = np.zeros((img.shape[0]+ 2*pad_x, img.shape[1] + 2*pad_y, 3), dtype=np.uint8 )
    container[pad_x: pad_x + img.shape[0], pad_y: pad_y + img.shape[1]] = img
    img = container

    pt1 = np.float32([[pad_x, pad_y],                           # Kiri atas
                      [pad_x + new_width, pad_y],               # Kanan atas
                      [pad_x, pad_y + new_height],              # Kiri bawah
                      [pad_x + new_width, pad_y + new_height]])  # Kanan bawah

    coeff = coeff
    pt2 = random_pt_1_side(pad_x, pad_y, new_width, new_height, coeff)
    M = cv2.getPerspectiveTransform(pt1, pt2)
    img = cv2.warpPerspective(img, M, (out_perspective_width, out_perspective_height))
    """Here we apply the random resize of the plate number"""
    scaling = np.random.uniform(0.4, 1.1)
    new_height = int(out_perspective_height * scaling)
    new_width = int(out_perspective_width * scaling)
    img = cv2.resize(img, (new_width, new_height))
    # ROTATE IMAGE
    # img = Image.fromarray(img)
    # img = img.rotate(np.random.randint(-30, 30), Image.NEAREST, True)
    """Find the edge of the plate"""
    temp = np.sum(img, axis=2)
    y_min, y_max, x_min, x_max = find_plate_edges(img)
    img = img[y_min:y_max, x_min:x_max]
    return img

def insert_plate_to_background(plate, index):
    """
    The output image will have 500 x 500
    - plate_coor = x1, y1, x2, y2
    This experiment have the false positive
    """

    """
    Update lagi, untuk pengambilan gambar dirandom berdasarkan pembacaan dari folder
    """
    # path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/background/"
    im_path = "./assets/background/"
    bg_images = os.listdir(im_path)
    # Random rotation
    z = 1
    while z == 1:
        try:
            path = os.path.join(im_path, np.random.choice(bg_images))
            bg = Image.open(path)
            bg = np.array(bg, dtype=np.uint8)
            z = 0

        except Exception as e:
            z = 1
            print('error read image', e)
            pass
    """
    SSD doesn't require crop the image, as it need the background
    remove the cropping, 
    Get the x, y coordinate
    """
    # Resize the image so it won't get any empty space
    bg = Image.fromarray(bg)
    bg = bg.resize((900, 900))

    # Rotating the image
    bg = bg.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    bg = np.array(bg)

    # Crop image to the center
    bg = bg[200:700, 200:700]

    # Resize the image to out_height x out_width
    bg = cv2.resize(bg, (out_width, out_height))
    """
    Plate placement on the background image is completely  random
    """

    num_plate = np.random.randint(1, 6)
    plate_coors = []
    status = []
    for m in range(num_plate):

        offset_y = 0 + np.random.randint(0, bg.shape[0] - 100)
        offset_x = 0 + np.random.randint(0, bg.shape[1] - 200)

        # Insert plate to the background
        x1, x2 = offset_x, offset_x + plate.shape[0]
        y1, y2 = offset_y, offset_y + plate.shape[1]

        temp = np.sum(plate, axis=2)

        # identifier untuk mendeteksi ada plat atau tidak
        # Plate are classified as exist, when number of pixels seen in sample reach 80% from plate
        seen = 0
        unseen = 0
        for (i, j), value in numpy.ndenumerate(temp):
            if temp[i, j] != 0:
                if x1+i < bg.shape[0] and y1+j < bg.shape[1]:
                    bg[x1+i, y1+j] = plate[i, j]
                    seen += 1
                else:
                    unseen += 1
                    # NULIS DI Y KALO INI TRUE KALO NGGAK BERARTI NANTI FALSE

        if seen/(seen+unseen) > 0.85:
            status.append(1)
        else:
            status.append(0)

        plate_coors.append((x1, y1, x2, y2))
    return bg, status, plate_coors


def number_plate_generator_single(max_number):
    s1 = 1
    s2 = 1
    s3 = 1

    if s1 + s2 + s3 == max_number:
        return s1, s2, s3

    s1 = np.random.randint(1, 2 + 1)
    max_s2 = max_number - s1 - 1
    if max_s2 > 4:
        max_s2 = 4
    s2 = np.random.randint(1, max_s2 + 1)
    max_s3 = max_number - s2 - s1
    s3 = max_number - s2 - s1
    if s3 > 3:
        s2 = s2 + (s3 - 3)
        s3 = 3
    if s2 > 4:
        s1 = s1 + (s2 - 4)
        s2 = 4

    return s1, s2, s3

def number_plate_generator_combination(num_per_class):
    """
    Membuat random plat nomor untuk tiap klas untuk menghindari classifier bias
    :param num_per_class: banyak data untuk tiap klas
    :return: arr of s1, s2, s3
    """
    numbers_per_class = np.zeros((num_per_class, num_class, 3))

    for i in range(num_per_class):
        """max_num sebagai counter class"""
        max_number = 3
        for j in range(num_class):
            s1, s2, s3 = number_plate_generator_single(max_number)

            numbers_per_class[i, j, 0] = s1
            numbers_per_class[i, j, 1] = s2
            numbers_per_class[i, j, 2] = s3

            max_number += 1
    return numbers_per_class


def find_center_w_h(plate_coor):
    X = plate.shape[0] / 2 + plate_coor[0]
    Y = plate.shape[1] / 2 + plate_coor[1]
    return X, Y


def find_grid_position(image_height, image_width, grid_width, grid_height, cell_id, threshold=0.2):
    """
    Find minimum and maximum location of the plate by selected grid
    :param image_height:
    :param image_width:
    :param grid_width:
    :param grid_height:
    :param cell_id:
    :param threshold:
    :return:
    """
    cell_width = int(image_width / grid_width)
    cell_height = int(image_height / grid_height)
    x1 = int(cell_id % grid_width) * cell_width
    y1 = int(cell_id / grid_width) * cell_height
    x2 = x1 + (threshold * cell_width)
    y2 = y1 + (threshold * cell_height)
    return x1, y1, x2, y2

def insert_plate_to_background_multi(plates):
    """
    Insert multiple plate to the background

    :param plates: list of plate
    :param param:
    :return:
    """
    # path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/background/"
    im_path = "./assets/background/"
    bg_images = os.listdir(im_path)
    # Random rotation
    z = 1
    while z == 1:
        try:
            path = os.path.join(im_path, np.random.choice(bg_images))
            bg = Image.open(path)
            bg = np.array(bg, dtype=np.uint8)
            z = 0

        except Exception as e:
            z = 1
            print('error read image', e)
            pass
    """Preprocess the background image"""
    bg = Image.fromarray(bg)
    bg = bg.resize((900, 900))

    # Rotating the image
    bg = bg.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    bg = np.array(bg)

    bg = bg[200:700, 200:700]  # Crop image to the center
    bg = cv2.resize(bg, (out_width, out_height))  # Resize the image to out_height x out_width
    """
    Plate placement on the background image is completely random on grid
    E.g Grid size 3 x 4
    Grid classing
    -------------
    | 0 | 1 | 2 |
    | 3 | 4 | 5 |
    | 6 | 7 | 8 |
    | 9 | 10| 11|
    ------------- 
    
    Grid size 2 x 2
    ---------
    | 0 | 1 |
    | 2 | 3 |
    ---------
    """
    grid_width = 3
    grid_height = 4
    grid_size = grid_height * grid_width
    cell_ids = []  # List of used location in grid
    plate_coors = []
    # print(len(plates))
    for m in range(len(plates)):
        plate = plates[m]

        """
        Inserting plates to the background
        PLEASE READ THE STORY MDFK
        """
        cell_id = None
        if cell_ids:  # There is a plate in image
            valid_cell = 0  # valid cell flag
            while not valid_cell:  # find the valid location
                prop_cell_id = np.random.randint(0, grid_size)  # propose random cell to be used
                if prop_cell_id in cell_ids:  # proposed cell id already used, so it's not valid
                    # print('proposed cell id is not valid')
                    valid_cell = 0
                else:  # proposed cell id haven't used so it's okay
                    # print('proposed location is valid', prop_cell_id)
                    cell_id = prop_cell_id
                    valid_cell = 1
            grid_x1, grid_y1, grid_x2, grid_y2 = find_grid_position(out_height, out_width, grid_width,
                                                                    grid_height, cell_id)
            offset_y = np.random.randint(grid_y1, grid_y2)
            offset_x = np.random.randint(grid_x1, grid_x2)
            x1, x2 = offset_x, offset_x + plate.shape[1]
            y1, y2 = offset_y, offset_y + plate.shape[0]

        else:  # No plate in the image, so all grid location are okay
            prop_cell_id = np.random.randint(0, grid_size)  # propose random cell to be used
            # print('proposed location is valid', cell_id)
            cell_id = prop_cell_id

            grid_x1, grid_y1, grid_x2, grid_y2 = find_grid_position(out_height, out_width, grid_width,
                                                                    grid_height, cell_id)
            offset_y = np.random.randint(grid_y1, grid_y2)
            offset_x = np.random.randint(grid_x1, grid_x2)
            x1, x2 = offset_x, offset_x + plate.shape[1]
            y1, y2 = offset_y, offset_y + plate.shape[0]

        # bg[y1:y2, x1:x2] = plate
        cell_ids.append(cell_id)  # add valid cell id to the cell ids
        class_id = 1
        plate_coors.append([x1, x2, y1, y2, class_id])
        temp = np.sum(plate, axis=2)
        for i in range(temp.shape[0]):
            for j in range(temp.shape[1]):
                if temp[i, j] != 0:
                    if y1+i < bg.shape[0] and x1+j < bg.shape[1]:
                        bg[y1+i, x1+j] = plate[i, j]
        # temp = np.sum(plate, axis=2)
        # for (i, j), value in numpy.ndenumerate(temp):
        #     if temp[i, j] != 0:
        #         if x1 + i < bg.shape[0] and y1 + j < bg.shape[1]:
        #             bg[x1 + i, y1 + j] = plate[i, j]
        # time.sleep(0.005)  # Delay to minimize error possibility
    return bg, plate_coors


if __name__ == '__main__':
    """
    ssd samples 5
    Creating samples for SSD
    >Change format and file saver using HDF5 
    """
    # Change the variable name to perspective
    out_perspective_width = 200
    out_perspective_height = 100

    # Real output of the image
    out_width = 640
    out_height = 480

    num_class = 7
    statistic = {
        '1': 0,
        '2': 0,
        '3': 0,
        '4': 0,
    }

    # Output directory from the generator
    root_dir = '.'
    output_dir = os.path.join(root_dir, 'generated', 'ssd_samples_6', 'dataset1')

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    height = 42
    width = 138

    """
    Generate kombinasi plat nomor sebanyak perhitungan ini
    num_samples = num_class * num_example
    """
    max_plate = 7  # Max plate per each sample
    num_samples = 100
    plate_number_combination = number_plate_generator_combination(int(num_samples*max_plate/num_class))
    plate_number_combination = plate_number_combination.reshape((-1, 3))  # Reshape jadi satu array
    np.random.shuffle(plate_number_combination)  # Shuffle the array

    """
    Labels for each image is
    status, x, y, h, w
    """
    labels = []
    images = []
    image_shapes = []
    label_shapes = []
    image_ids = []
    # images = np.zeros((num_samples, out_height, out_width))

    id_plate = 0  # counter for plate_number_combination
    pd_labels = pd.DataFrame()
    for index in range(num_samples):
        plates = []
        num_plate = np.random.randint(1, max_plate + 1)
        for pc in range(num_plate):  # pc = plate counter
            s1 = int(plate_number_combination[id_plate][0])
            s2 = int(plate_number_combination[id_plate][1])
            s3 = int(plate_number_combination[id_plate][2])
            s1, s2, s3 = generate_number.generate_plate_number(s1, s2, s3)
            plate_number = s1 + "  " + s2 + "  " + s3  # Penambahan spasi disini

            plate = create_plate_background(height, width)
            plate = insert_text_to_plate_background(plate, plate_number, str(np.random.randint(0, 99)) + '.' + str(np.random.randint(0, 99)))
            plate = modify_image_plate2(plate, 20, 10)
            plates.append(plate)
            id_plate += 1

        image, plate_coors = insert_plate_to_background_multi(plates)

        # Add noise to image output
        mean = np.random.randint(1, 25)
        std = np.random.randint(0, mean)
        image = image + np.random.normal(mean, std, image.shape)
        image = np.clip(image, 0, 255).astype(np.uint8)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Save the image
        image_filename = os.path.join(output_dir, str(index) + ".jpg")
        cv2.imwrite(image_filename, image)

        # Append to be pickled
        # Image not pickled
        char_count = len(s1) + len(s2) + len(s3)
        image_shapes.append(image.shape)
        image = image.reshape(-1)
        images.append(image)
        # images[index] = image.reshape(-1)
        plate_coors = np.array(plate_coors)
        plate_coors = plate_coors.reshape(-1, plate_coors.shape[-1])   # Make the plate_coors 2 dimensional instead of 3
        plate_coors = np.insert(plate_coors, 0, index, axis=-1)  # Add image_id to each of the plate_coors
        labels.extend(plate_coors)  # Use extend instead of append
        label_shapes.append(plate_coors.shape)
        image_ids.append(image_filename)

        # time.sleep(0.01)  # Delay to minimize error possibility

        statistic[str(len(s2))] += 1
        print('Created %d from %d' % (index+1, num_samples))


    # Save in CSV
    df = pd.DataFrame(data=labels, columns=['image_id', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id'])
    df['image_id'] = df['image_id'].map(lambda x: str(x) + '.jpg')
    df.to_csv(os.path.join(output_dir, 'labels.csv'), index=False)
    # Save the statistic of each class
    # todo implement the per character statistic
    filename = 'statistic.txt'
    with open(os.path.join(output_dir, filename), 'w') as f:
        f.write(str(statistic))
    f.close()