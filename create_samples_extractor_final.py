import os
import numpy as np
import pandas as pd
from create_sample_helper_2 import *


def style_1(plate_img):
    return plate_img


def style_2(plate_img):
    plate_img = modify_image_plate_rotation(plate_img)
    plate_img = insert_plate_to_background_extractor(plate_img, varians=0.02)
    return plate_img

def style_3(plate_img):
    plate_img = modify_image_plate(plate_img, 75, 17, (300, 500), 1)
    plate_img = insert_plate_to_background_extractor(plate_img, varians=0.02)
    return plate_img

def style_4(plate_img):
    plate_img = modify_image_plate_full(plate_img, 75, 17, (300, 500), 1)
    plate_img = insert_plate_to_background_extractor(plate_img, varians=0.02)
    return plate_img

if __name__ == '__main__':
    """
    Create samples for anything
    only create plat number without any extra processing
    
    This one is new, 
    will only output the labels and image, not generating single dataset file
    
    NEW:
    have random posibilities for creating different sample style
    ##STYLE LIST##
    1. plain plate
    2. plain + random rotation
    3. plain + random perspective transform
    4. plain + random rotation + random perspective transform  (gak work?)
    """

    root = '.'
    # work_dir = os.path.join(root, 'generated', 'create_sample_extractor_final', 'test_dataset')
    work_dir = '/mnt/drive1/skripsi/dataset/augmented'
    if not os.path.exists(work_dir):
        os.makedirs(work_dir)

    num_samples = 15000
    index_start = 1
    num_class = 7

    plate_number_combination = number_plate_generator_combination(int(num_samples / num_class) + 1).reshape((-1, 3))
    np.random.shuffle(plate_number_combination)

    plates = []
    plate_shapes = []
    label_s1 = [[], [], []]  # s1_0, s1_1, s1_2
    label_s2 = [[], [], [], [], []]  # s2_0, s2_1, s2_2, s2_3, s2_4
    label_s3 = [[], [], [], []]  # s3_0, s3_1, s3_2, s3_3
    image_ids = []

    for index in range(num_samples):
        plate_width = np.random.randint(99, 138)
        plate_height = 42
        s1_0 = int(plate_number_combination[index][0])
        s2_0 = int(plate_number_combination[index][1])
        s3_0 = int(plate_number_combination[index][2])
        s1, s2, s3 = generate_plate_number(s1_0, s2_0, s3_0)
        plate_number = (str(s1), str(s2), str(s3))  # plate number combination
        # plate_number = ('AA', '1111', 'AAA')  # Force to have this text
        plate_img = create_plate_background(plate_height, plate_width)
        plate_img = insert_text_to_plate_background(plate_img, plate_number,
                                                    str(np.random.randint(0, 99)) + '.' + str(np.random.randint(0, 99)),
                                                    screw=None)

        # plate_img = style_4(plate_img)
        "Random transformation"
        rand_trans = np.random.randint(0, 4)
        if rand_trans is 0:
            plate_img = style_1(plate_img)
        elif rand_trans is 1:
            plate_img = style_2(plate_img)
        elif rand_trans is 2:
            plate_img = style_3(plate_img)
        elif rand_trans is 3:
            plate_img = style_4(plate_img)

        # Add noise to image output
        mean = np.random.randint(1, 20)
        std = np.random.randint(0, mean)
        plate_img = plate_img + np.random.normal(mean, std, plate_img.shape)
        plate_img = np.clip(plate_img, 0, 255).astype(np.uint8)

        curr_index = index+index_start
        filename = str(curr_index) + ".jpg"
        image_filename = os.path.join(work_dir, filename)
        cv2.imwrite(image_filename, plate_img)  # save .img file

        "add to labels file"
        image_ids.append(filename)
        label_s1[0].append(s1_0)
        for i in range(2):
            try:
                label_s1[i + 1].append(s1[i])
            except Exception as e:
                label_s1[i + 1].append("")
                pass

        label_s2[0].append(s2_0)
        for i in range(4):
            try:
                label_s2[i + 1].append(s2[i])
            except Exception as e:
                label_s2[i + 1].append("")
                pass

        label_s3[0].append(s3_0)
        for i in range(3):
            try:
                label_s3[i + 1].append(s3[i])
            except Exception as e:
                label_s3[i + 1].append("")
                pass

        print("Finished ", index + 1, " of ", num_samples)

    df1 = pd.DataFrame(image_ids, columns=['image_filename'])
    df2 = pd.DataFrame(data={'s1_0': label_s1[0],
                             's1_1': label_s1[1],
                             's1_2': label_s1[2]})

    df3 = pd.DataFrame(data={'s2_0': label_s2[0],
                             's2_1': label_s2[1],
                             's2_2': label_s2[2],
                             's2_3': label_s2[3],
                             's2_4': label_s2[4]})

    df4 = pd.DataFrame(data={'s3_0': label_s3[0],
                             's3_1': label_s3[1],
                             's3_2': label_s3[2],
                             's3_3': label_s3[3]})

    final_labels = pd.concat([df1, df2, df3, df4], axis=1)
    final_labels.to_csv(os.path.join(work_dir, 'labels.csv'), index=False)

