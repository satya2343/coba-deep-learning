import random
import string
import numpy
import numpy as np
import cv2 as cv2
from PIL import ImageFont, ImageDraw, Image
import os


def create_plate_background(height, width):
    # Create canvas
    background = np.zeros((height, width, 3), np.uint8)
    c = np.random.randint(2, 100)  # color of the plate
    color = np.array([c, c, c], np.uint8)
    background += color
    return background

def insert_text_to_plate_background(plate_background, plate_number, year, screw='random'):
    s1, s2, s3 = plate_number

    font_path = "./assets/IndonesiaLicensePlate.ttf"
    font_size = int(plate_background.shape[1] / 4.5) + np.random.randint(0, 5)
    font = ImageFont.truetype(font_path, font_size)

    # Year font
    font_size2 = 14
    font2 = ImageFont.truetype(font_path, font_size2)

    c = np.random.randint(120, 255)
    color = (c, c, c, 0)

    # create canvas
    img = Image.fromarray(plate_background)
    draw = ImageDraw.Draw(img)

    # Get the width of the text for text alignment
    xs1, ys1 = draw.textsize(s1, font=font)
    xs2, ys2 = draw.textsize(s2, font=font)
    xs3, ys3 = draw.textsize(s3, font=font)
    xs4, ys4 = draw.textsize(year, font=font2)
    """
        ======================
        |        y1          |
        |x2(s1)x1(s2)x1(s3)x2|
        |        y2          |
        |x3(year)            |
        ======================
    x1 = plate spacing with plate width
    x2 = text spacing
    x3 = year spacing
    y1 = plate number top spacing
    y2 = year top spacing 
    """
    x1 = int(np.random.randint(2, ((plate_background.shape[1] - (xs1 + xs2 + xs3)) / 2) + 3 ))  # 2 as min spacing
    x2 = int((plate_background.shape[1] - (xs1 + xs2 + xs3) - (2*x1)) / 2)
    x3 = int(np.random.randint(2, (plate_background.shape[1] - xs4) + 3))  # 2 as min spacing

    y1 = int(np.random.randint(1, int(0.1*plate_background.shape[0])))
    y2 = int(np.random.randint(1, plate_background.shape[0] - y1 - ys1 - ys4))

    draw.text((x2, y1), str(s1), font=font, fill=color)  # draw s1
    draw.text((x2 + xs1 + x1, y1), str(s2), font=font, fill=color)  # draw s2
    draw.text((x2 + xs1 + x1 + xs2 + x1, y1), str(s3), font=font, fill=color)  # draw s3
    draw.text((x3, y1 + ys1 + y2), str(year), font=font2, fill=color)

    img = np.array(img)
    if screw:
        sy1 = int(plate_background.shape[0] * np.random.uniform(0.2, 0.8))  # screw distance from top plate
        sy2 = sy1
        sx1 = int(plate_background.shape[1] * np.random.uniform(0.15, 0.4))  # screw distance from left plate
        sx2 = plate_background.shape[1] - sx1
        screw = create_screw(np.random.randint(6, 12))
        # Paste screw to the plate
        temp = np.sum(screw, axis=2)
        for i in range(temp.shape[0]):
            for j in range(temp.shape[1]):
                if temp[i, j] != 0:
                    if sy1 + i < img.shape[0] and sx1 + j < img.shape[1]:
                        img[sy1+i, sx1+j] = screw[i, j]

        for i in range(temp.shape[0]):
            for j in range(temp.shape[1]):
                if temp[i, j] != 0:
                    if sy2 + i < img.shape[0] and sx2 + j < img.shape[1]:
                        img[sy2+i, sx2+j] = screw[i, j]

    elif screw == 'random':
        pass
    # pad image with white random border
    img = np.pad(img, ((1, 1), (1, 1), (0, 0)), 'constant', constant_values=np.random.randint(0, 255))

    return img

def create_screw(size):
    # Create canvas
    img = np.zeros((size, size, 3), np.uint8)

    # Draw screw
    c = np.random.randint(120, 255)  # color of the screw
    cv2.circle(img, (int(size/2), int(size/2)), int(size/2), (c, c, c), thickness=-1, lineType=cv2.LINE_4)

    # Draw rectangle
    c = np.random.randint(120, 255)  # color of the screw
    x1 = np.random.randint(int(size * 0.2), int(size * 0.4))
    x2 = size - x1
    y1 = np.random.randint(int(size * 0.2), int(size * 0.4))
    y2 = size - y1
    cv2.rectangle(img, (x1, y1), (x2, y2), (c, c, c), lineType=cv2.LINE_4, thickness=-1)
    img = Image.fromarray(img)
    img = img.rotate(np.random.randint(-180, 180), Image.NEAREST, True)
    img = np.array(img)
    return img

def random_pt_1_side(pad_x=None, pad_y=None, new_width=None, new_height=None, coeff=None, rand_coor=None):

    coeff_x = coeff
    coeff_y = coeff
    if not rand_coor:
        rand_coor = np.random.randint(1, 4)

    if rand_coor == 1:
        pt2 = np.float32([[pad_x + np.random.randint(-coeff_x, coeff_x), pad_y + np.random.randint(-coeff_y, coeff_y)],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif rand_coor == 2:
        pt2 = np.float32([[pad_x , pad_y],
                          [pad_x + new_width + np.random.randint(-coeff_x, coeff_x), pad_y + np.random.randint(-coeff_y, coeff_y)],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif rand_coor == 3:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x + np.random.randint(-coeff_x, coeff_x), pad_y + new_height + np.random.randint(-coeff_y, coeff_y)],
                          [pad_x + new_width, pad_y + new_height]])
    else:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width + np.random.randint(-coeff_x, coeff_x), pad_y + new_height + np.random.randint(-coeff_y, coeff_y)]])
    return pt2

def modify_image_plate_full(plate, pad, coeff, output, side=None):
    out_perspective_height = output[0]
    out_perspective_width = output[1]
    width, height, channels = plate.shape
    img = Image.fromarray(plate)

    # SCALE IMAGE
    scaling = np.random.uniform(0.9, 1.7)
    new_height = int(height * scaling)
    new_width = int(width * scaling)
    img = img.resize((new_height, new_width))

    img = np.array(img)

    # ROTATE IMAGE
    img = Image.fromarray(img)
    img = img.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    img = np.array(img)

    # APPLYING SKEWING TRANSFORM :V

    """
    Skewing transformation menggunakan nilai coeff yang random untuk tiap sisi
    SSD transformation need variant size of the image
    In real data it will represent the range of the plate 
    """
    # Pad the image
    pad_x = pad
    pad_y = pad

    container = np.zeros((img.shape[0] + 2 * pad_x, img.shape[1] + 2 * pad_y, 3), dtype=np.uint8)
    container[pad_x: pad_x + img.shape[0], pad_y: pad_y + img.shape[1]] = img
    img = container

    pt1 = np.float32([[pad_x, pad_y],  # Kiri atas
                      [pad_x + new_width, pad_y],  # Kanan atas
                      [pad_x, pad_y + new_height],  # Kiri bawah
                      [pad_x + new_width, pad_y + new_height]])  # Kanan bawah

    coeff = coeff
    pt2 = random_pt_1_side(pad_x, pad_y, new_width, new_height, coeff, side)
    M = cv2.getPerspectiveTransform(pt1, pt2)
    img = cv2.warpPerspective(img, M, (out_perspective_width, out_perspective_height))
    """Here we apply the random resize of the plate number"""
    scaling = np.random.uniform(0.5, 1.2)
    new_height = int(out_perspective_height * scaling)
    new_width = int(out_perspective_width * scaling)
    img = cv2.resize(img, (new_width, new_height))
    """Find the edge of the plate"""
    temp = np.sum(img, axis=2)
    y_min, y_max, x_min, x_max = find_plate_edges(img)
    img = img[y_min:y_max, x_min:x_max]

    # Add blur
    # img = cv2.GaussianBlur(img, (5, 5), 0)
    # img = cv2.GaussianBlur(img, (3, 3), 0)
    return img

def modify_image_plate(plate, pad, coeff, output, side=None):
    out_perspective_height = output[0]
    out_perspective_width = output[1]
    width, height, channels = plate.shape
    img = Image.fromarray(plate)

    # SCALE IMAGE
    scaling = np.random.uniform(0.9, 1.7)
    new_height = int(height * scaling)
    new_width = int(width * scaling)
    img = img.resize((new_height, new_width))

    img = np.array(img)
    # APPLYING SKEWING TRANSFORM :V

    """
    Skewing transformation menggunakan nilai coeff yang random untuk tiap sisi
    SSD transformation need variant size of the image
    In real data it will represent the range of the plate 
    """
    # Pad the image
    pad_x = pad
    pad_y = pad

    container = np.zeros((img.shape[0] + 2 * pad_x, img.shape[1] + 2 * pad_y, 3), dtype=np.uint8)
    container[pad_x: pad_x + img.shape[0], pad_y: pad_y + img.shape[1]] = img
    img = container

    pt1 = np.float32([[pad_x, pad_y],  # Kiri atas
                      [pad_x + new_width, pad_y],  # Kanan atas
                      [pad_x, pad_y + new_height],  # Kiri bawah
                      [pad_x + new_width, pad_y + new_height]])  # Kanan bawah

    coeff = coeff
    pt2 = random_pt_1_side(pad_x, pad_y, new_width, new_height, coeff, side)
    M = cv2.getPerspectiveTransform(pt1, pt2)
    img = cv2.warpPerspective(img, M, (out_perspective_width, out_perspective_height))
    """Here we apply the random resize of the plate number"""
    scaling = np.random.uniform(0.5, 1.2)
    new_height = int(out_perspective_height * scaling)
    new_width = int(out_perspective_width * scaling)
    img = cv2.resize(img, (new_width, new_height))
    # ROTATE IMAGE
    # img = Image.fromarray(img)
    # img = img.rotate(np.random.randint(-30, 30), Image.NEAREST, True)
    """Find the edge of the plate"""
    temp = np.sum(img, axis=2)
    y_min, y_max, x_min, x_max = find_plate_edges(img)
    img = img[y_min:y_max, x_min:x_max]

    # Add blur
    # img = cv2.GaussianBlur(img, (5, 5), 0)
    # img = cv2.GaussianBlur(img, (3, 3), 0)
    return img

def modify_image_plate_rotation(plate):
    plate = Image.fromarray(plate)
    plate = plate.rotate(np.random.randint(-30, 30), Image.NEAREST, True)
    return np.array(plate)


def find_edge_point(temp, edge):
    w = temp.shape[1]
    h = temp.shape[0]
    # Find y_min
    if edge == 'y_min':
        for i in range(h):
            for j in range(w):
                if temp[i, j] != 0:
                    y_min = temp[i, j]
                    return i
    # Find y_max
    if edge == 'y_max':
        for i in reversed(range(h)):
            for j in reversed(range(w)):
                if temp[i, j] != 0:
                    y_max = temp[i, j]
                    return i
    # Find x_min
    if edge == 'x_min':
        for j in range(w):
            for i in range(h):
                if temp[i, j] != 0:
                    x_min = temp[i, j]
                    return j
    # Find x_max
    if edge == 'x_max':
        for j in reversed(range(w)):
            for i in reversed(range(h)):
                if temp[i, j] != 0:
                    x_max = temp[i, j]
                    return j

def find_plate_edges(img):
    img = np.array(img)
    temp = np.sum(img, axis=2)

    y_min = find_edge_point(temp, 'y_min')
    x_min = find_edge_point(temp, 'x_min')
    y_max = find_edge_point(temp, 'y_max')
    x_max = find_edge_point(temp, 'x_max')

    return y_min, y_max, x_min, x_max

def insert_plate_to_background_extractor(plate, varians=0.2):
    """
    Add background for the number extractor sampler
    :param plate:
    :param varians: Output varians as error. by padding the backgrounded image with the percentage of plate dimension
    :return:
    """

    """
    Process the new plate by varians
    """
    pady1 = int(plate.shape[0] * np.random.uniform(0, varians))
    pady2 = int(plate.shape[0] * np.random.uniform(0, varians))
    padx1 = int(plate.shape[1] * np.random.uniform(0, varians))
    padx2 = int(plate.shape[1] * np.random.uniform(0, varians))
    plate = np.pad(plate, [(pady1, pady2), (padx1, padx2), (0, 0)], mode='constant')
    im_path = "./assets/background/"
    bg_images = os.listdir(im_path)
    # Random rotation
    z = 1
    while z == 1:
        try:
            path = os.path.join(im_path, np.random.choice(bg_images))
            bg = Image.open(path)
            bg = np.array(bg, dtype=np.uint8)
            z = 0

        except Exception as e:
            z = 1
            print('error read image', e)
            pass
    """
    Background are first to be processed, so that there won't be any similarity for each sample
    """
    # Resize the image so it won't get any empty space
    bg = Image.fromarray(bg)
    bg = bg.resize((900, 900))

    # Rotating the image
    bg = bg.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    bg = np.array(bg)

    bg = bg[200:700, 200:700]  # Crop image to the center

    # Crop image at random area by new dimension size
    y1 = np.random.randint(0, 150)
    y2 = y1 + plate.shape[0]
    x1 = np.random.randint(0, 150)
    x2 = x1 + plate.shape[1]
    bg = bg[y1:y2, x1:x2]

    # Insert the plate to the background
    temp = np.sum(plate, axis=2)
    for i in range(temp.shape[0]):
        for j in range(temp.shape[1]):
            if temp[i, j] != 0:
                if i < bg.shape[0] and j < bg.shape[1]:
                    bg[i, j] = plate[i, j]

    return bg

def insert_plate_to_background(plate, output):
    """
    The output image will have 500 x 500
    - plate_coor = x1, y1, x2, y2
    This experiment have the false positive
    """

    """
    Update lagi, untuk pengambilan gambar dirandom berdasarkan pembacaan dari folder
    """
    # path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/background/"
    im_path = "./assets/background/"
    bg_images = os.listdir(im_path)
    # Random rotation
    z = 1
    while z == 1:
        try:
            path = os.path.join(im_path, np.random.choice(bg_images))
            bg = Image.open(path)
            bg = np.array(bg, dtype=np.uint8)
            z = 0

        except Exception as e:
            z = 1
            print('error read image', e)
            pass
    """
    SSD doesn't require crop the image, as it need the background
    remove the cropping, 
    Get the x, y coordinate
    """
    # Resize the image so it won't get any empty space
    bg = Image.fromarray(bg)
    bg = bg.resize((900, 900))

    # Rotating the image
    bg = bg.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    bg = np.array(bg)

    # Crop image to the center
    bg = bg[200:700, 200:700]

    # Resize the image to out_height x out_width
    bg = cv2.resize(bg, output)  # Output of the image
    """
    Plate placement on the background image is completely  random
    Give the chance that the plate number won't be exist on the image
    """

    # Give offset the CHANCE for not including plate on the output image
    offset_y = 0 + np.random.randint(0, bg.shape[0] - 50)
    offset_x = 0 + np.random.randint(0, bg.shape[1] - 100)

    # Insert plate to the background
    x1, x2 = offset_x, offset_x + plate.shape[0]
    y1, y2 = offset_y, offset_y + plate.shape[1]

    temp = np.sum(plate, axis=2)

    # identifier untuk mendeteksi ada plat atau tidak
    # Plate are classified as exist, when number of pixels seen in sample reach 80% from plate
    seen = 0
    unseen = 0
    status = 0
    for (i, j), value in numpy.ndenumerate(temp):
        if temp[i, j] != 0:
            if x1+i < bg.shape[0] and y1+j < bg.shape[1]:
                bg[x1+i, y1+j] = plate[i, j]
                seen += 1
            else:
                unseen += 1
                # NULIS DI Y KALO INI TRUE KALO NGGAK BERARTI NANTI FALSE

    if seen/(seen+unseen) > 0.85:
        status = 1
    else:
        status = 0

    plate_coor = (x1, y1, x2, y2)
    return bg, status, plate_coor

def insert_plate_to_background_multi(plates, output):
    """
    The output image will have 500 x 500
    - plate_coor = x1, y1, x2, y2
    This experiment have the false positive
    """

    """
    Update lagi, untuk pengambilan gambar dirandom berdasarkan pembacaan dari folder
    """
    # path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/background/"
    im_path = "./assets/background/"
    bg_images = os.listdir(im_path)
    # Random rotation
    z = 1
    while z == 1:
        try:
            path = os.path.join(im_path, np.random.choice(bg_images))
            bg = Image.open(path)
            bg = np.array(bg, dtype=np.uint8)
            z = 0

        except Exception as e:
            z = 1
            print('error read image', e)
            pass
    """
    SSD doesn't require crop the image, as it need the background
    remove the cropping, 
    Get the x, y coordinate
    """
    # Resize the image so it won't get any empty space
    bg = Image.fromarray(bg)
    bg = bg.resize((900, 900))

    # Rotating the image
    bg = bg.rotate(np.random.randint(-20, 20), Image.NEAREST, True)
    bg = np.array(bg)

    # Crop image to the center
    bg = bg[200:700, 200:700]

    # Resize the image to out_height x out_width
    bg = cv2.resize(bg, output)  # Output of the image

    plate_coors = []
    for m in range(len(plates)):
        plate = plates[m]
        """
        Plate placement on the background image is completely  random
        Give the chance that the plate number won't be exist on the image
        """

        offset_y = 0 + np.random.randint(0, bg.shape[0] - plate.shape[0])
        offset_x = 0 + np.random.randint(0, bg.shape[1] - plate.shape[1])

        # Insert plate to the background
        x1, x2 = offset_x, offset_x + plate.shape[0]
        y1, y2 = offset_y, offset_y + plate.shape[1]

        temp = np.sum(plate, axis=2)

        for (i, j), value in numpy.ndenumerate(temp):
            if temp[i, j] != 0:
                if x1+i < bg.shape[0] and y1+j < bg.shape[1]:
                    bg[x1+i, y1+j] = plate[i, j]

        plate_coors.append((x1, y1, x2, y2))
    bg = np.array(bg)
    return bg, plate_coors

def number_plate_generator_single(max_number):
    s1 = 1
    s2 = 1
    s3 = 1

    if s1 + s2 + s3 == max_number:
        return s1, s2, s3

    s1 = np.random.randint(1, 2 + 1)
    max_s2 = max_number - s1 - 1
    if max_s2 > 4:
        max_s2 = 4
    s2 = np.random.randint(1, max_s2 + 1)
    max_s3 = max_number - s2 - s1
    s3 = max_number - s2 - s1
    if s3 > 3:
        s2 = s2 + (s3 - 3)
        s3 = 3
    if s2 > 4:
        s1 = s1 + (s2 - 4)
        s2 = 4

    return s1, s2, s3

def number_plate_generator_combination(num_per_class, num_class=7):
    """
    Membuat random plat nomor untuk tiap klas untuk menghindari classifier bias
    :param num_per_class: banyak data untuk tiap klas
    :return: arr of s1, s2, s3
    """
    numbers_per_class = np.zeros((num_per_class, num_class, 3))

    for i in range(num_per_class):
        """max_num sebagai counter class"""
        max_number = 3
        for j in range(num_class):
            s1, s2, s3 = number_plate_generator_single(max_number)

            numbers_per_class[i, j, 0] = s1
            numbers_per_class[i, j, 1] = s2
            numbers_per_class[i, j, 2] = s3

            max_number += 1
    return numbers_per_class


def find_center_w_h(plate, plate_coor):
    X = plate.shape[0] / 2 + plate_coor[0]
    Y = plate.shape[1] / 2 + plate_coor[1]
    return X, Y

def generate_plate_number(m1, m2, m3):
    s1 = random_char(m1)  # kode daerah
    s2 = random_number(m2)  # kode angka random
    s3 = random_char(m3)  # karakter random belakang
    return s1, s2, s3


def random_char(n):
    s = ""
    for i in range(n):
        s += random.choice(string.ascii_uppercase)
    return s


def random_number(n):
    s = ""
    for i in range(n):
        s += str(random.randint(0, 9))
    return s