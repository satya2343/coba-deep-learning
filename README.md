Detecting Plate Number In The Wild Using Convolutional Neural Network
====================
In this research we use 2 separate model to solve the problem. First model is Localization model which used to find the plate number coordinates. Second model is Extraction model which used to predict the character from the plate number.
We use SSD as our backbone in localization model. Extraction model is designed to extract the information by calculating all possible character from indonesian plate number efficiently.
By using this method, we managed to achieve 89% accuracy in localization model by using 45 testing samples and 600 training samples. In the Extraction model we achieve 87% accuracy in 300 testing samples and 1400 training samples(very small). 



