import numpy
import numpy as np
import generate_number
import cv2 as cv2
import matplotlib.pyplot as plt
from PIL import ImageFont, ImageDraw, Image
from six.moves import cPickle as pickle
import os
import pandas as pd


def create_plate_background(height, width):
    # CREATE CANVAS
    background = np.zeros((height, width, 3), np.uint8)
    c = np.random.randint(2, 100)
    color = np.array([c, c, c], np.uint8)
    background += color
    return background


def insert_text_to_plate_background(plate_background, plate_number, *year):
    font_path = "/mnt/drive1/Kuliah/8/skripsi/ekperimen/generate_training_samples/assets/IndonesiaLicensePlate.ttf"
    font_size = 32
    font = ImageFont.truetype(font_path, font_size)
    c = np.random.randint(120, 255)
    color = (c, c, c, 0)

    # CREATE IMAGE
    img = Image.fromarray(plate_background)
    draw = ImageDraw.Draw(img)

    # Get the width of the text for text alignment
    text_size_x, text_size_y = draw.textsize(plate_number, font=font)
    # Center the plate number text
    draw.text(((width - text_size_x) / 2, 2), plate_number, font=font, fill=color)

    if year[0]:
        font_size = 14
        font = ImageFont.truetype(font_path, font_size)

        # Get the width of the text for text alignment
        text_size_x, text_size_y = draw.textsize(plate_number, font=font)
        # randomize the year number text location between left, center, right
        draw.text((np.random.randint(5, (width - text_size_x)), 28), str(year[0]), font=font, fill=color)

    # PAD IMAGE WITH ALL WHITE
    img = np.array(img)
    img = np.pad(img, ((1, 1), (1, 1), (0, 0)), 'constant', constant_values=255)

    return img


def modify_image_plate(plate):
    width, height, channels = plate.shape
    img = Image.fromarray(plate)

    # SCALE IMAGE
    scaling = np.random.uniform(0.7, 1.5)
    new_height = int(height * scaling)
    new_width = int(width * scaling)
    img = img.resize((new_height, new_width))

    img = np.array(img)
    # APPLYING SKEWING TRANSFORM :V

    # Pad the image
    pad_x = 120
    pad_y = 120

    container = np.zeros((img.shape[0] + 2 * pad_x, img.shape[1] + 2 * pad_y, 3), dtype=np.uint8)
    container[pad_x: pad_x + img.shape[0], pad_y: pad_y + img.shape[1]] = img
    img = container

    pt1 = np.float32([[pad_x, pad_y],  # Kiri atas
                      [pad_x + new_width, pad_y],  # Kanan atas
                      [pad_x, pad_y + new_height],  # Kiri bawah
                      [pad_x + new_width, pad_y + new_height]])  # Kanan bawah

    coeff = 30
    pt2 = random_pt_1_side(pad_x, pad_y, new_width, new_height, coeff)
    M = cv2.getPerspectiveTransform(pt1, pt2)
    img = cv2.warpPerspective(img, M, (400, 400))

    # Rotate the image
    # img = Image.fromarray(img)
    # img = img.rotate(np.random.randint(-30, 30), Image.NEAREST, True)

    img = np.array(img, dtype=np.uint8)
    return img

def random_pt_1_side(pad_x=None, pad_y=None, new_width=None, new_height=None, coeff=None):

    if np.random.randint(1, 4) == 1:
        pt2 = np.float32([[pad_x + np.random.randint(-30, 30), pad_y + np.random.randint(-30, 30)],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif np.random.randint(1, 4) == 2:
        pt2 = np.float32([[pad_x , pad_y],
                          [pad_x + new_width + np.random.randint(-30, 30), pad_y + np.random.randint(-30, 30)],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width, pad_y + new_height]])
    elif np.random.randint(1, 4) == 3:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x + np.random.randint(-30, 30), pad_y + new_height + np.random.randint(-30, 30)],
                          [pad_x + new_width, pad_y + new_height]])
    else:
        pt2 = np.float32([[pad_x, pad_y],
                          [pad_x + new_width, pad_y],
                          [pad_x, pad_y + new_height],
                          [pad_x + new_width + np.random.randint(-30, 30), pad_y + new_height + np.random.randint(-30, 30)]])
    return pt2

if __name__ == '__main__':
    num_samples = 1000

    height = 42
    width = 128

    # Label terdiri dari 2 jenis yaitu
    # Nomor plat dan status terlihat atau tidak
    # PAKAI STATUS KALAU NANTI MEMANG MEMERLUKAN NEGATIVE FALSE
    # KALAU TIDAK PAKAI MENDING GA USAH DIPAKAI
    labels = dict()
    label_asal_kendaraan = list()
    label_plat_kendaraan = list()
    label_random_plat = list()

    # Images terdiri dari num_samples, width, height, 3 channel
    images = np.zeros((num_samples, 400, 400), dtype=np.int8)

    # Output directory from the generator
    root_dir = '.'
    output_dir = os.path.join(root_dir, 'output_dataset_plat_nomor_test')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for index in range(num_samples):
        s1, s2, s3 = generate_number.generate_plate_number(np.random.randint(1, 3), 4, np.random.randint(2, 4))
        plate_number = s1 + " " + s2 + " " + s3

        plate = create_plate_background(height, width)
        plate = insert_text_to_plate_background(plate, plate_number,
                                                str(np.random.randint(0, 99)) + '.' + str(np.random.randint(0, 99)))
        plate = modify_image_plate(plate)
        plate = cv2.cvtColor(plate, cv2.COLOR_BGR2GRAY)

        # Set the label based on plate number and status
        label_asal_kendaraan.append(s1)
        label_plat_kendaraan.append(s2)
        label_random_plat.append(s3)

        # Save the image
        cv2.imwrite(os.path.join(output_dir, str(index) + ".jpg"), plate)

        # Prepare image data for pickle saving
        images[index] = plate

        print('creating %d of %d' % (index+1, num_samples))

    # Save the label data in pickle
    labels['asal_kendaraan'] = label_asal_kendaraan
    labels['plat_kendaraan'] = label_plat_kendaraan
    labels['random_plat'] = label_random_plat

    filename = 'labels.pickle'
    with open(os.path.join(output_dir, filename), 'wb') as f:
        pickle.dump(labels, f, pickle.HIGHEST_PROTOCOL)

    # Save the images data in the pickle
    filename = 'dataset.pickle'
    with open(os.path.join(output_dir, filename), 'wb') as f:
        pickle.dump(images, f, pickle.HIGHEST_PROTOCOL)