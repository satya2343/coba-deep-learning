import os
import tensorflow as tf
import cv2 as cv2
import numpy as np
import sys
import h5py
import pandas as pd

def serialize_array(arr):
    arr = np.array(arr).tostring()
    return np.void(arr)

if __name__ == '__main__':
    """
    Create single HDF5 dataset from curret root folder
    The dataset will contain image and labels in single output file
    """

    root = '/mnt/drive1/skripsi/dataset/training3/'
    # root = 'E:/skripsi/dataset/test2'
    output_filename = 'dataset.h5'

    # Read .csv label file
    label_filename = 'labels.csv'
    labels = pd.read_csv(os.path.join(root, label_filename))
    labels = labels.fillna('')  # Remove nan
    labels = labels.sample(frac=1)  # Random shuffling the dataset. AWAS
    serialized_image = []  # save the serialized image as string from np.tostring()
    image_shapes = []  # save image shapes

    for i, label in enumerate(labels.iterrows()):
        # Read image using cv2
        image = cv2.imread(os.path.join(root, label[1]['image_filename']))

        # Save image shape
        image_shapes.append(image.shape)

        # serialize as single array
        serialized_image.append(image.reshape(-1))

        # Print some information
        sys.stdout.flush()
        sys.stdout.write('\rloaded ' + str(i+1) + ' from ' + str(len(labels)))

    # Save as single HDF5 dataset
    hf = h5py.File(os.path.join(root, output_filename))
    print(' ')
    print('Creating single dataset file...')

    # If image hasn't resized yet
    # f_image = hf.create_dataset('images', data=serialized_image, maxshape=(None), dtype=h5py.special_dtype(vlen=np.uint8))

    # Image already resized
    f_image = hf.create_dataset('images', data=serialized_image)

    f_shape = hf.create_dataset('image_shapes', data=image_shapes)
    labels = labels.drop(columns='image_filename')
    f_label = hf.create_dataset('labels', data=np.array(labels), dtype=h5py.special_dtype(vlen=str))

    hf.close()
    print('Done...')