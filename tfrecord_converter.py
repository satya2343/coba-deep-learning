import os
import tensorflow as tf
import pandas as pd
import cv2 as cv2
import numpy as np
import sys

# Helperfunctions to make feature definition more readable
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

if __name__ == '__main__':
    """
    just use the np.tostring() => doesn't need to know the dimension of the image
    """
    root = '/mnt/drive1/skripsi/dataset/ready/humanity/100-1/ekstraktor'
    labels = pd.read_csv(os.path.join(root, 'labels.csv'))
    # labels = labels.replace(np.nan, '', regex=True)
    labels = labels.fillna('')
    output_file = 'dataset.tfrecord'
    output_file = os.path.join(root, output_file)

    # Write each image into tfrecord
    with tf.python_io.TFRecordWriter(output_file) as writer:
        i = 0
        # From CSV read all image then save to .tfrecord
        for label in labels.iterrows():
            # Read image
            image = cv2.imread(os.path.join(root, label[1]['image_filename']))
            # Convert to string then add to the np array
            image = image.tostring()
            data = {
                'image': _bytes_feature(image),
                's1_0': _int64_feature(np.array(label[1]['s1_0'])),
                's1_1': _bytes_feature(label[1]['s1_1'].encode()),
                's1_2': _bytes_feature(label[1]['s1_2'].encode()),
                's2_0': _int64_feature(label[1]['s2_0']),
                's2_1': _int64_feature(label[1]['s2_1']),
                's2_2': _int64_feature(label[1]['s2_2']),
                's2_3': _int64_feature(label[1]['s2_3']),
                's2_4': _int64_feature(label[1]['s2_4']),
                's3_0': _int64_feature(label[1]['s3_0']),
                's3_1': _bytes_feature(label[1]['s3_1'].encode()),
                's3_2': _bytes_feature(label[1]['s3_2'].encode()),
                's3_3': _bytes_feature(label[1]['s3_2'].encode()),
            }
            # Wrap into tf feature
            feature = tf.train.Features(feature=data)
            # Wrap into tf example
            example = tf.train.Example(features=feature)
            # Serialize data
            serialized = example.SerializeToString()
            writer.write(serialized)
            sys.stdout.flush()
            sys.stdout.write('\rloaded ' + str(i) + ' from ' + str(len(labels)))
            i += 1
