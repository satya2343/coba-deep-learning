import numpy as np
import random
import string


def generate_plate_number(m1, m2, m3):
    s1 = random_char(m1)  # kode daerah
    s2 = random_number(m2)  # kode angka random
    s3 = random_char(m3)  # karakter random belakang
    return s1, s2, s3


def random_char(n):
    s = ""
    for i in range(n):
        s += random.choice(string.ascii_uppercase)
    return s


def random_number(n):
    s = ""
    for i in range(n):
        s += str(random.randint(0, 9))
    return s