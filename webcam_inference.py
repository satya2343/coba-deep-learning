"""
Inference with webcam
"""

import cv2 as cv2
import numpy as np

if __name__ == '__main__':

    video = cv2.VideoCapture(0)
    # ret, frame = video.read()
    # video.release()
    # img = np.array(frame)
    # cv2.imshow('frame', frame)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    while True:
        ret, frame = video.read()  # Get video per frame
        """
        Inferencing goes in here
        """
        # TODO: Inferencing
        # Deep learning Prediction
        # Process the image
        # Show processed image
        image = cv2.imshow('frame', frame)

        key = cv2.waitKey(1)
        if key == ord('q'):
            break

    video.release()
    cv2.destroyAllWindows()